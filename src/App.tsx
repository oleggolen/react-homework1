import React, {useState} from 'react';
import {QuerySenderClass} from "./Components/QuerySenderClass";
import {ResultOutput} from "./Components/ResultOutput";
import {QuerySenderFunctional} from "./Components/QuerySenderFunctional";


function App() {
    let [result, setResult] = useState<string>("");
    let [isSuccess, setIsSuccess] = useState<boolean>(true);
    let [componentType, setComponentType] = useState<boolean>(true);

    function resultProcessing(result: string, isSuccess: boolean) {
        setResult(result);
        setIsSuccess(isSuccess)
    }

    const classSelected = () => {
        setComponentType(true);
        setResult("");
    }
    const functionalSelected = () => {
        setComponentType(false);
        setResult("");
    }

    return (
        <div>
            <div>
                <label>Классовые компоненты</label><input type={"radio"} id={"class"} name={"radio"}
                                                          onChange={classSelected}/>
                <label>Функциональные компоненты</label><input type={"radio"} id={"functional"} name={"radio"}
                                                               onChange={functionalSelected}/>
            </div>
            <div>
                { componentType ? <QuerySenderClass resultProcessing={resultProcessing}/> : <QuerySenderFunctional resultProcessing={resultProcessing} /> }
                <ResultOutput result={result} isSuccess={isSuccess}/>
            </div>
        </div>);

}
export default App;
