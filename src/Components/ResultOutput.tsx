import {Component} from "react";
import {ResultOutputProps} from "../Common/ResultOutputProps";
import './TextStyles.css'
export class ResultOutput extends Component<ResultOutputProps>{
    render() {
        if(!this.props.isSuccess){
            return <div className={"redText"}>
                {this.props.result}
            </div>
        }
        return <div className={"blackText"}>
            {this.props.result}
        </div>
    }
}