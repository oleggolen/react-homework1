import {FormEvent, useState} from "react";
import {QuerySenderProps} from "../Common/QuerySenderProps";
import axios from "axios";
import './QuerySenderStyle.css'
export function QuerySenderFunctional(props: QuerySenderProps) {
    let [address, setAddress] = useState<string>("");
    const onInput = (event: FormEvent<HTMLInputElement>) => {
        setAddress(event.currentTarget.value);

    }
    const onClick = () => {
        try {
            axios({method: "GET", url: address}).then(value => {
                if (value.status >= 200 || value.status <= 299) {
                    const response = value.data;
                    props.resultProcessing(JSON.stringify(response), true);
                } else {
                    props.resultProcessing(value.statusText, false);
                }
            }).catch(reason => {
                props.resultProcessing(reason.message, false);
            })
        }
        catch{
            props.resultProcessing('error', false)
        }

    }

    return <div>
        <p>Функциональная компонента</p>
        <input type={"text"}  value={address} onInput={onInput}/>
        <button  onClick={onClick}>Отправить</button>
    </div>;
}