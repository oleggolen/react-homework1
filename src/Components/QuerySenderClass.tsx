import {Component, FormEvent} from "react";
import {QuerySenderProps} from "../Common/QuerySenderProps";
import {QuerySenderState} from "../Common/QuerySenderState";
import './QuerySenderStyle.css'
import axios from "axios";

export  class QuerySenderClass extends Component<QuerySenderProps, QuerySenderState>{
    constructor(props: QuerySenderProps) {
        super(props);
        this.state = {
            address: ""
        }
    }
    onInput = (event: FormEvent<HTMLInputElement>) => {
        this.setState({address: event.currentTarget.value});

    }
    onClick = () => {
        try {
            axios({method: "GET", url: this.state.address}).then(value => {
                if (value.status >= 200 || value.status <= 299) {
                    const response = value.data;
                    this.props.resultProcessing(JSON.stringify(response), true);
                } else {
                    this.props.resultProcessing(value.statusText, false);
                }
            }).catch(reason => {
                this.props.resultProcessing(reason.message, false);
            })
        }
        catch{
            this.props.resultProcessing('error', false)
        }

    }
    render() {
        return <div>
            <p>Классовая компонента</p>
            <input type={"text"}  value={this.state.address} onInput={this.onInput}/>
            <button  onClick={this.onClick}>Отправить</button>
        </div>;
    }
}