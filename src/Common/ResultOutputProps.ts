export interface ResultOutputProps{
    result: string;
    isSuccess: boolean;
}