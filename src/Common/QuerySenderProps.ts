export interface QuerySenderProps{
    resultProcessing: (result: string, isSuccess: boolean) => void;
}